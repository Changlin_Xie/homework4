﻿namespace _5092_homework4
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Stock_price = new System.Windows.Forms.Label();
            this.Strike_price = new System.Windows.Forms.Label();
            this.riskFreerate = new System.Windows.Forms.Label();
            this.vol1 = new System.Windows.Forms.Label();
            this.tenor1 = new System.Windows.Forms.Label();
            this.times = new System.Windows.Forms.Label();
            this.step = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.Stockprice = new System.Windows.Forms.TextBox();
            this.vol = new System.Windows.Forms.TextBox();
            this.Kprice = new System.Windows.Forms.TextBox();
            this.rate = new System.Windows.Forms.TextBox();
            this.Tenor = new System.Windows.Forms.TextBox();
            this.Trials = new System.Windows.Forms.TextBox();
            this.steps = new System.Windows.Forms.TextBox();
            this.cores_n = new System.Windows.Forms.TextBox();
            this.antithetic = new System.Windows.Forms.CheckBox();
            this.delta_c = new System.Windows.Forms.CheckBox();
            this.mutithread = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.call_theta1 = new System.Windows.Forms.TextBox();
            this.call_gamma1 = new System.Windows.Forms.TextBox();
            this.call_delta1 = new System.Windows.Forms.TextBox();
            this.call_price1 = new System.Windows.Forms.TextBox();
            this.call_rho1 = new System.Windows.Forms.TextBox();
            this.call_vega1 = new System.Windows.Forms.TextBox();
            this.call_se1 = new System.Windows.Forms.TextBox();
            this.call_price = new System.Windows.Forms.Label();
            this.call_delta = new System.Windows.Forms.Label();
            this.call_gamma = new System.Windows.Forms.Label();
            this.call_theta = new System.Windows.Forms.Label();
            this.call_rho = new System.Windows.Forms.Label();
            this.call_vega = new System.Windows.Forms.Label();
            this.call_se = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.put_se1 = new System.Windows.Forms.TextBox();
            this.put_vega1 = new System.Windows.Forms.TextBox();
            this.put_rho1 = new System.Windows.Forms.TextBox();
            this.put_price1 = new System.Windows.Forms.TextBox();
            this.put_delta1 = new System.Windows.Forms.TextBox();
            this.put_gamma1 = new System.Windows.Forms.TextBox();
            this.put_theta1 = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.runTime = new System.Windows.Forms.TextBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.button1 = new System.Windows.Forms.Button();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // Stock_price
            // 
            this.Stock_price.AutoSize = true;
            this.Stock_price.Location = new System.Drawing.Point(47, 152);
            this.Stock_price.Name = "Stock_price";
            this.Stock_price.Size = new System.Drawing.Size(87, 15);
            this.Stock_price.TabIndex = 0;
            this.Stock_price.Text = "stockprice";
            // 
            // Strike_price
            // 
            this.Strike_price.AutoSize = true;
            this.Strike_price.Location = new System.Drawing.Point(47, 194);
            this.Strike_price.Name = "Strike_price";
            this.Strike_price.Size = new System.Drawing.Size(87, 15);
            this.Strike_price.TabIndex = 1;
            this.Strike_price.Text = "strkeprice";
            // 
            // riskFreerate
            // 
            this.riskFreerate.AutoSize = true;
            this.riskFreerate.Location = new System.Drawing.Point(33, 241);
            this.riskFreerate.Name = "riskFreerate";
            this.riskFreerate.Size = new System.Drawing.Size(103, 15);
            this.riskFreerate.TabIndex = 2;
            this.riskFreerate.Text = "riskfreerate";
            // 
            // vol1
            // 
            this.vol1.AutoSize = true;
            this.vol1.Location = new System.Drawing.Point(47, 279);
            this.vol1.Name = "vol1";
            this.vol1.Size = new System.Drawing.Size(87, 15);
            this.vol1.TabIndex = 3;
            this.vol1.Text = "volatility";
            // 
            // tenor1
            // 
            this.tenor1.AutoSize = true;
            this.tenor1.Location = new System.Drawing.Point(47, 317);
            this.tenor1.Name = "tenor1";
            this.tenor1.Size = new System.Drawing.Size(47, 15);
            this.tenor1.TabIndex = 4;
            this.tenor1.Text = "tenor";
            // 
            // times
            // 
            this.times.AutoSize = true;
            this.times.Location = new System.Drawing.Point(47, 356);
            this.times.Name = "times";
            this.times.Size = new System.Drawing.Size(55, 15);
            this.times.TabIndex = 5;
            this.times.Text = "trials";
            // 
            // step
            // 
            this.step.AutoSize = true;
            this.step.Location = new System.Drawing.Point(47, 395);
            this.step.Name = "step";
            this.step.Size = new System.Drawing.Size(47, 15);
            this.step.TabIndex = 6;
            this.step.Text = "steps";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(195, 450);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 15);
            this.label8.TabIndex = 7;
            this.label8.Text = "cores";
            // 
            // Stockprice
            // 
            this.Stockprice.Location = new System.Drawing.Point(142, 149);
            this.Stockprice.Name = "Stockprice";
            this.Stockprice.Size = new System.Drawing.Size(100, 25);
            this.Stockprice.TabIndex = 8;
            this.Stockprice.Text = "50";
            this.Stockprice.TextChanged += new System.EventHandler(this.Stockprice_TextChanged);
            // 
            // vol
            // 
            this.vol.Location = new System.Drawing.Point(142, 279);
            this.vol.Name = "vol";
            this.vol.Size = new System.Drawing.Size(100, 25);
            this.vol.TabIndex = 9;
            this.vol.Text = "0.5";
            this.vol.TextChanged += new System.EventHandler(this.vol_TextChanged);
            // 
            // Kprice
            // 
            this.Kprice.Location = new System.Drawing.Point(142, 194);
            this.Kprice.Name = "Kprice";
            this.Kprice.Size = new System.Drawing.Size(100, 25);
            this.Kprice.TabIndex = 10;
            this.Kprice.Text = "60";
            this.Kprice.TextChanged += new System.EventHandler(this.Kprice_TextChanged);
            // 
            // rate
            // 
            this.rate.Location = new System.Drawing.Point(142, 238);
            this.rate.Name = "rate";
            this.rate.Size = new System.Drawing.Size(100, 25);
            this.rate.TabIndex = 11;
            this.rate.Text = "0.05";
            this.rate.TextChanged += new System.EventHandler(this.rate_TextChanged);
            // 
            // Tenor
            // 
            this.Tenor.Location = new System.Drawing.Point(142, 317);
            this.Tenor.Name = "Tenor";
            this.Tenor.Size = new System.Drawing.Size(100, 25);
            this.Tenor.TabIndex = 12;
            this.Tenor.Text = "1";
            this.Tenor.TextChanged += new System.EventHandler(this.Tenor_TextChanged);
            // 
            // Trials
            // 
            this.Trials.Location = new System.Drawing.Point(142, 353);
            this.Trials.Name = "Trials";
            this.Trials.Size = new System.Drawing.Size(100, 25);
            this.Trials.TabIndex = 13;
            this.Trials.Text = "10000";
            this.Trials.TextChanged += new System.EventHandler(this.Trials_TextChanged);
            // 
            // steps
            // 
            this.steps.Location = new System.Drawing.Point(142, 395);
            this.steps.Name = "steps";
            this.steps.Size = new System.Drawing.Size(100, 25);
            this.steps.TabIndex = 14;
            this.steps.Text = "50";
            this.steps.TextChanged += new System.EventHandler(this.steps_TextChanged);
            // 
            // cores_n
            // 
            this.cores_n.Location = new System.Drawing.Point(277, 443);
            this.cores_n.Name = "cores_n";
            this.cores_n.Size = new System.Drawing.Size(100, 25);
            this.cores_n.TabIndex = 15;
            // 
            // antithetic
            // 
            this.antithetic.AutoSize = true;
            this.antithetic.Location = new System.Drawing.Point(34, 21);
            this.antithetic.Name = "antithetic";
            this.antithetic.Size = new System.Drawing.Size(109, 19);
            this.antithetic.TabIndex = 16;
            this.antithetic.Text = "Antithetic";
            this.antithetic.UseVisualStyleBackColor = true;
            this.antithetic.CheckedChanged += new System.EventHandler(this.antithetic_CheckedChanged);
            // 
            // delta_c
            // 
            this.delta_c.AutoSize = true;
            this.delta_c.Location = new System.Drawing.Point(34, 68);
            this.delta_c.Name = "delta_c";
            this.delta_c.Size = new System.Drawing.Size(125, 19);
            this.delta_c.TabIndex = 17;
            this.delta_c.Text = "Deltacontrol";
            this.delta_c.UseVisualStyleBackColor = true;
            // 
            // mutithread
            // 
            this.mutithread.AutoSize = true;
            this.mutithread.Location = new System.Drawing.Point(34, 112);
            this.mutithread.Name = "mutithread";
            this.mutithread.Size = new System.Drawing.Size(109, 19);
            this.mutithread.TabIndex = 18;
            this.mutithread.Text = "Mutithread";
            this.mutithread.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(420, 98);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(39, 15);
            this.label9.TabIndex = 19;
            this.label9.Text = "call";
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(756, 98);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(31, 15);
            this.label10.TabIndex = 20;
            this.label10.Text = "put";
            // 
            // call_theta1
            // 
            this.call_theta1.Location = new System.Drawing.Point(400, 279);
            this.call_theta1.Name = "call_theta1";
            this.call_theta1.Size = new System.Drawing.Size(100, 25);
            this.call_theta1.TabIndex = 21;
            // 
            // call_gamma1
            // 
            this.call_gamma1.Location = new System.Drawing.Point(400, 231);
            this.call_gamma1.Name = "call_gamma1";
            this.call_gamma1.Size = new System.Drawing.Size(100, 25);
            this.call_gamma1.TabIndex = 22;
            // 
            // call_delta1
            // 
            this.call_delta1.Location = new System.Drawing.Point(400, 191);
            this.call_delta1.Name = "call_delta1";
            this.call_delta1.Size = new System.Drawing.Size(100, 25);
            this.call_delta1.TabIndex = 23;
            // 
            // call_price1
            // 
            this.call_price1.Location = new System.Drawing.Point(400, 142);
            this.call_price1.Name = "call_price1";
            this.call_price1.Size = new System.Drawing.Size(100, 25);
            this.call_price1.TabIndex = 24;
            // 
            // call_rho1
            // 
            this.call_rho1.Location = new System.Drawing.Point(400, 317);
            this.call_rho1.Name = "call_rho1";
            this.call_rho1.Size = new System.Drawing.Size(100, 25);
            this.call_rho1.TabIndex = 25;
            // 
            // call_vega1
            // 
            this.call_vega1.Location = new System.Drawing.Point(400, 353);
            this.call_vega1.Name = "call_vega1";
            this.call_vega1.Size = new System.Drawing.Size(100, 25);
            this.call_vega1.TabIndex = 26;
            // 
            // call_se1
            // 
            this.call_se1.Location = new System.Drawing.Point(400, 392);
            this.call_se1.Name = "call_se1";
            this.call_se1.Size = new System.Drawing.Size(100, 25);
            this.call_se1.TabIndex = 27;
            // 
            // call_price
            // 
            this.call_price.AutoSize = true;
            this.call_price.Location = new System.Drawing.Point(317, 152);
            this.call_price.Name = "call_price";
            this.call_price.Size = new System.Drawing.Size(47, 15);
            this.call_price.TabIndex = 29;
            this.call_price.Text = "price";
            // 
            // call_delta
            // 
            this.call_delta.AutoSize = true;
            this.call_delta.Location = new System.Drawing.Point(317, 204);
            this.call_delta.Name = "call_delta";
            this.call_delta.Size = new System.Drawing.Size(47, 15);
            this.call_delta.TabIndex = 30;
            this.call_delta.Text = "delta";
            // 
            // call_gamma
            // 
            this.call_gamma.AutoSize = true;
            this.call_gamma.Location = new System.Drawing.Point(317, 241);
            this.call_gamma.Name = "call_gamma";
            this.call_gamma.Size = new System.Drawing.Size(47, 15);
            this.call_gamma.TabIndex = 31;
            this.call_gamma.Text = "gamma";
            // 
            // call_theta
            // 
            this.call_theta.AutoSize = true;
            this.call_theta.Location = new System.Drawing.Point(317, 282);
            this.call_theta.Name = "call_theta";
            this.call_theta.Size = new System.Drawing.Size(47, 15);
            this.call_theta.TabIndex = 32;
            this.call_theta.Text = "theta";
            // 
            // call_rho
            // 
            this.call_rho.AutoSize = true;
            this.call_rho.Location = new System.Drawing.Point(317, 320);
            this.call_rho.Name = "call_rho";
            this.call_rho.Size = new System.Drawing.Size(31, 15);
            this.call_rho.TabIndex = 33;
            this.call_rho.Text = "rho";
            // 
            // call_vega
            // 
            this.call_vega.AutoSize = true;
            this.call_vega.Location = new System.Drawing.Point(317, 356);
            this.call_vega.Name = "call_vega";
            this.call_vega.Size = new System.Drawing.Size(39, 15);
            this.call_vega.TabIndex = 34;
            this.call_vega.Text = "vega";
            // 
            // call_se
            // 
            this.call_se.AutoSize = true;
            this.call_se.Location = new System.Drawing.Point(317, 402);
            this.call_se.Name = "call_se";
            this.call_se.Size = new System.Drawing.Size(23, 15);
            this.call_se.TabIndex = 35;
            this.call_se.Text = "SE";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(644, 395);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(23, 15);
            this.label18.TabIndex = 49;
            this.label18.Text = "SE";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(644, 349);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(39, 15);
            this.label19.TabIndex = 48;
            this.label19.Text = "vega";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(644, 313);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(31, 15);
            this.label20.TabIndex = 47;
            this.label20.Text = "rho";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(644, 275);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(47, 15);
            this.label21.TabIndex = 46;
            this.label21.Text = "theta";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(644, 234);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(47, 15);
            this.label22.TabIndex = 45;
            this.label22.Text = "gamma";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(644, 197);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(47, 15);
            this.label23.TabIndex = 44;
            this.label23.Text = "delta";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(644, 145);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(47, 15);
            this.label24.TabIndex = 43;
            this.label24.Text = "price";
            this.label24.Click += new System.EventHandler(this.label24_Click);
            // 
            // put_se1
            // 
            this.put_se1.Location = new System.Drawing.Point(727, 385);
            this.put_se1.Name = "put_se1";
            this.put_se1.Size = new System.Drawing.Size(100, 25);
            this.put_se1.TabIndex = 42;
            // 
            // put_vega1
            // 
            this.put_vega1.Location = new System.Drawing.Point(727, 346);
            this.put_vega1.Name = "put_vega1";
            this.put_vega1.Size = new System.Drawing.Size(100, 25);
            this.put_vega1.TabIndex = 41;
            // 
            // put_rho1
            // 
            this.put_rho1.Location = new System.Drawing.Point(727, 310);
            this.put_rho1.Name = "put_rho1";
            this.put_rho1.Size = new System.Drawing.Size(100, 25);
            this.put_rho1.TabIndex = 40;
            // 
            // put_price1
            // 
            this.put_price1.Location = new System.Drawing.Point(727, 135);
            this.put_price1.Name = "put_price1";
            this.put_price1.Size = new System.Drawing.Size(100, 25);
            this.put_price1.TabIndex = 39;
            // 
            // put_delta1
            // 
            this.put_delta1.Location = new System.Drawing.Point(727, 184);
            this.put_delta1.Name = "put_delta1";
            this.put_delta1.Size = new System.Drawing.Size(100, 25);
            this.put_delta1.TabIndex = 38;
            // 
            // put_gamma1
            // 
            this.put_gamma1.Location = new System.Drawing.Point(727, 224);
            this.put_gamma1.Name = "put_gamma1";
            this.put_gamma1.Size = new System.Drawing.Size(100, 25);
            this.put_gamma1.TabIndex = 37;
            // 
            // put_theta1
            // 
            this.put_theta1.Location = new System.Drawing.Point(727, 272);
            this.put_theta1.Name = "put_theta1";
            this.put_theta1.Size = new System.Drawing.Size(100, 25);
            this.put_theta1.TabIndex = 36;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(420, 443);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(71, 15);
            this.label25.TabIndex = 50;
            this.label25.Text = "run time";
            this.label25.Click += new System.EventHandler(this.label25_Click);
            // 
            // runTime
            // 
            this.runTime.Location = new System.Drawing.Point(527, 440);
            this.runTime.Name = "runTime";
            this.runTime.Size = new System.Drawing.Size(100, 25);
            this.runTime.TabIndex = 51;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(693, 440);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(220, 23);
            this.progressBar1.TabIndex = 52;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(384, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(261, 69);
            this.button1.TabIndex = 53;
            this.button1.Text = "Compute";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1001, 524);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.runTime);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.put_se1);
            this.Controls.Add(this.put_vega1);
            this.Controls.Add(this.put_rho1);
            this.Controls.Add(this.put_price1);
            this.Controls.Add(this.put_delta1);
            this.Controls.Add(this.put_gamma1);
            this.Controls.Add(this.put_theta1);
            this.Controls.Add(this.call_se);
            this.Controls.Add(this.call_vega);
            this.Controls.Add(this.call_rho);
            this.Controls.Add(this.call_theta);
            this.Controls.Add(this.call_gamma);
            this.Controls.Add(this.call_delta);
            this.Controls.Add(this.call_price);
            this.Controls.Add(this.call_se1);
            this.Controls.Add(this.call_vega1);
            this.Controls.Add(this.call_rho1);
            this.Controls.Add(this.call_price1);
            this.Controls.Add(this.call_delta1);
            this.Controls.Add(this.call_gamma1);
            this.Controls.Add(this.call_theta1);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.mutithread);
            this.Controls.Add(this.delta_c);
            this.Controls.Add(this.antithetic);
            this.Controls.Add(this.cores_n);
            this.Controls.Add(this.steps);
            this.Controls.Add(this.Trials);
            this.Controls.Add(this.Tenor);
            this.Controls.Add(this.rate);
            this.Controls.Add(this.Kprice);
            this.Controls.Add(this.vol);
            this.Controls.Add(this.Stockprice);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.step);
            this.Controls.Add(this.times);
            this.Controls.Add(this.tenor1);
            this.Controls.Add(this.vol1);
            this.Controls.Add(this.riskFreerate);
            this.Controls.Add(this.Strike_price);
            this.Controls.Add(this.Stock_price);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Stock_price;
        private System.Windows.Forms.Label Strike_price;
        private System.Windows.Forms.Label riskFreerate;
        private System.Windows.Forms.Label vol1;
        private System.Windows.Forms.Label tenor1;
        private System.Windows.Forms.Label times;
        private System.Windows.Forms.Label step;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox Stockprice;
        private System.Windows.Forms.TextBox vol;
        private System.Windows.Forms.TextBox Kprice;
        private System.Windows.Forms.TextBox rate;
        private System.Windows.Forms.TextBox Tenor;
        private System.Windows.Forms.TextBox Trials;
        private System.Windows.Forms.TextBox steps;
        private System.Windows.Forms.TextBox cores_n;
        private System.Windows.Forms.CheckBox antithetic;
        private System.Windows.Forms.CheckBox delta_c;
        private System.Windows.Forms.CheckBox mutithread;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox call_theta1;
        private System.Windows.Forms.TextBox call_gamma1;
        private System.Windows.Forms.TextBox call_delta1;
        private System.Windows.Forms.TextBox call_price1;
        private System.Windows.Forms.TextBox call_rho1;
        private System.Windows.Forms.TextBox call_vega1;
        private System.Windows.Forms.TextBox call_se1;
        private System.Windows.Forms.Label call_price;
        private System.Windows.Forms.Label call_delta;
        private System.Windows.Forms.Label call_gamma;
        private System.Windows.Forms.Label call_theta;
        private System.Windows.Forms.Label call_rho;
        private System.Windows.Forms.Label call_vega;
        private System.Windows.Forms.Label call_se;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox put_se1;
        private System.Windows.Forms.TextBox put_vega1;
        private System.Windows.Forms.TextBox put_rho1;
        private System.Windows.Forms.TextBox put_price1;
        private System.Windows.Forms.TextBox put_delta1;
        private System.Windows.Forms.TextBox put_gamma1;
        private System.Windows.Forms.TextBox put_theta1;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox runTime;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ErrorProvider errorProvider1;
    }
}

