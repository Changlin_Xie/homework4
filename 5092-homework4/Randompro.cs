﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace _5092_homework4
{
    class Randompro
    {
        public static int cores = Environment.ProcessorCount;
        private int trials;
        private int N;
     
        public Randompro(int trials, int N)
        {
            this.trials = trials;
            this.N = N;
        }
        // use box-muller to generate the random number.
        public static double box_muller(Random rnd)
        {
            double x1, x2, z1, z2;
            x1 = rnd.NextDouble();
            x2 = rnd.NextDouble();
            z1 = Math.Sqrt(-2 * Math.Log(x1)) * Math.Cos(2 * Math.PI * x2);
            z2 = Math.Sqrt(-2 * Math.Log(x1)) * Math.Sin(2 * Math.PI * x2);
            return z1;
        }
        
        public void randomnum()      // this is for not muti
        {
            Random rnd = new Random();
            for (int i = 0; i <= trials - 1; i++)
            {
                for (int j = 0; j <= N - 1; j++)
                {
                    Form1.ran[i, j] = box_muller(rnd);             
                }
            }
        }
       
        public void multithread() //this is for muti
        {
            
            int trials1;
            if (trials % (cores - 1) != 0)
            {
                trials1 = trials + (cores - 1 - trials % (cores - 1));
            }
            else
            {
                trials1 = trials;
            }
            int perc = trials1 / (cores - 1);
            // create a function which include the process of  generating the random number.
            Action<object> myACt = x => {
                long para = Convert.ToInt32(x);
                Random rnd = new Random((int)DateTime.Now.Ticks);
                long start = para;
                long end =Math.Min ( para + perc,trials);
                for (long i = start; i < end; i++)
                {
                    for (int j = 0; j <= N - 1; j++)
                    {
                        Form1.ran[i, j] = box_muller(rnd);
                    }
                }
            };
            int count = 0;
            List<Thread> ThreadList = new List<Thread>();
      
            for (int i = 0; i < cores-1; i++)
            {
                Thread th = new Thread(new ParameterizedThreadStart(myACt));
                ThreadList.Add(th);
                th.Start(count);
                count = count + perc;
            }
            
                foreach (Thread th in ThreadList)
                {
                    th.Join();
                }
         
            foreach (Thread th in ThreadList)
            {
                th.Abort();
            }

        }
    }
}
